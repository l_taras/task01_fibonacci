import exception.ClosableException;
import exception.FibonacciException;

import java.util.ArrayList;

public class Fibonacci implements AutoCloseable {
    private int n;
    private ArrayList<Integer> list = new ArrayList<Integer>();

    public Fibonacci(int f1, int f2, int n) throws FibonacciException {
        if (n < 3) {
            throw new FibonacciException();
        }
        list.add(f1);
        list.add(f2);
        for (int i = 3; i <= n; i++) {
            int current = f1 + f2;
            list.add(current);
            f1 = f2;
            f2 = current;
        }
        this.n = n;
    }

    public int sumOdd() {
        int sumOdd = 0;
        for (Integer integer : this.list) {
            if (integer % 2 != 0) {
                sumOdd += integer;
            }
        }
        return sumOdd;
    }

    public int sumEven() {
        int sumEven = 0;
        for (Integer integer : this.list) {
            if (integer % 2 == 0) {
                sumEven += integer;
            }
        }
        return sumEven;
    }

    public void percentage() {
        float a = (float) sumOdd() / ((float) sumEven() + (float) sumOdd());
        float b = (float) sumEven() / ((float) sumEven() + (float) sumOdd());
        System.out.printf("%,2.2f %% Odd Percantage", a * 100);
        System.out.println();
        System.out.printf("%,2.2f %% Even Percantage", b * 100);
    }


    public int getN() {
        return n;
    }

    public ArrayList<Integer> getList() {
        return list;
    }

    public void setList(ArrayList<Integer> list) {
        this.list = list;
    }

    @Override
    public void close() throws ClosableException {
        System.out.println("\nFibonacci object is closed");
        throw new ClosableException();
    }
}
