package exception;

public class RangeException extends Exception {
    public final void showMessage() {
        System.out.println("\n Range inputs are invalid");
    }
}
