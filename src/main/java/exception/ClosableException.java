package exception;

public class ClosableException extends Exception {
    public final void showMessage() {
        System.out.println("\n Closable exception");
    }
}
