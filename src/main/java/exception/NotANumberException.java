package exception;

public class NotANumberException extends Exception {
    public final void showMessage() {
        System.out.println("\n Not a  number");
    }
}
