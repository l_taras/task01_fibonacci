
import exception.ClosableException;
import exception.FibonacciException;
import exception.NotANumberException;
import exception.RangeException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    private Main() {
    }

    public static int consoleInput(String text) throws NotANumberException {
        Scanner sc = new Scanner(System.in);
        int a = 0;
        System.out.println(text);
        if (!sc.hasNextInt()) {
            throw new NotANumberException();
        }
        a = sc.nextInt();
        return a;
    }


    public static void main(String[] args) {
        int begin = -1;
        int end = 0;
        int oddSum = 0;
        int evenSum = 0;
        ArrayList<Integer> odd = new ArrayList<Integer>();
        ArrayList<Integer> even = new ArrayList<Integer>();
        int f1 = 0;
        int f2 = 0;
        int n = 0;

        while (true) {
            try {
                begin = consoleInput("Please enter the beginning of the interval: ");
                break;
            } catch (Exception e) {
                System.err.println("Not a number");
            }
        }

        do {
            try {
                end = consoleInput("Please enter the end of the interval: ");
                try {
                    if (begin >= end) {
                        throw new RangeException();
                    }
                } catch (RangeException e) {
                    System.err.println("{" + begin + ";" + end + "} is not a correct range");
                }
            } catch (Exception e) {
                System.err.println("Not a number");
            }
        }
        while (begin >= end);


        for (int i = begin; i <= end; i++) {
            if (i % 2 != 0) {
                odd.add(i);
                oddSum += i;
            } else {
                even.add(i);
                evenSum += i;
            }
        }
        printCollection(odd);
        System.out.println();

        f1 = odd.get(odd.size() - 1);
        f2 = even.get(even.size() - 1);

        Collections.reverse(even);
        printCollection(even);

        System.out.println();
        System.out.println("sum of odd= " + oddSum);
        System.out.println("sum of even= " + evenSum);

        do {
            try {
                n = consoleInput("Please enter the size of Fibonacci numbers >3: ");
                try {
                    if (n < 3) {
                        throw new FibonacciException();
                    }
                } catch (FibonacciException e) {
                    System.err.println("can't create Fibonacci: too low range");
                }
            } catch (Exception e) {
                System.err.println("Not a number");
            }
        } while (n < 3);

        try (Fibonacci fi = new Fibonacci(f1, f2, n)) {
            printCollection(fi.getList());
            System.out.println();
            System.out.println("Odd sum= " + fi.sumOdd());
            System.out.println("Even sum= " + fi.sumEven());
            fi.percentage();
            throw new Exception();
        } catch (ClosableException e) {
            System.out.println("Can't close Fibonacci ClosableException");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Catch exception");
            e.printStackTrace();
        }
    }

    private static void printCollection(ArrayList<Integer> list) {
        for (Integer arr : list) {
            System.out.print(arr + " ");
        }
    }
}
